package com.example.calculadora;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnTouchModeChangeListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity implements OnTouchListener { // INTERFACE

	EditText edtR;
	Button btnMais;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		edtR = (EditText) findViewById(R.id.edtResultado);
		btnMais = (Button) findViewById(R.id.btnMais);
		btnMais.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (event.getAction() == MotionEvent.ACTION_UP) {
					edtR.setText(edtR.getText().toString() + "+");

				}
				return false;
			}
		});

		if (savedInstanceState != null) {
			String valorsalvo = savedInstanceState.getString("resultado");

			edtR.setText(valorsalvo);
		}
	}

	private OnTouchListener listener = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if (event.getAction() == MotionEvent.ACTION_UP) {
				edtR.setText(edtR.getText().toString() + "+");

			}
			return false;
		}
	};

	public void numberClick(View v) {
		edtR.setText(edtR.getText().toString() + ((Button) v).getText());

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if (event.getAction() == MotionEvent.ACTION_UP) {
			edtR.setText(edtR.getText().toString() + "+");

		}
		return false;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putString("resultado", "teste");
		super.onSaveInstanceState(outState);

	}

}
