package com.example.helloworld;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class TelaDois extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Log.i("HELLOWORD", "<TELA DOIS> Passo no ONCREATE");
		setContentView(R.layout.teladois);
	}
	
	@Override
    protected void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    	Log.i("HELLOWORD", "<TELA DOIS> Passo no ONSTART");
    }
    
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	Log.i("HELLOWORD", "<TELA DOIS> Passo no ONRESUME");
    }
    
    @Override
    protected void onPause() {
    	// TODO Auto-generated method stub
    	super.onPause();
    	Log.i("HELLOWORD", "<TELA DOIS> Passo no ONPAUSE");
    }
    
    @Override
    protected void onStop() {
    	// TODO Auto-generated method stub
    	super.onStop();
    	Log.i("HELLOWORD", "<TELA DOIS> Passo no ONSTOP");
    }
    
    @Override
    protected void onRestart() {
    	// TODO Auto-generated method stub
    	super.onRestart();
    	Log.i("HELLOWORD", "<TELA DOIS> Passo no ONRESTART");
    }
    
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    	Log.i("HELLOWORD", "<TELA DOIS> Passo no ONDESTROY");
    }
	
}
