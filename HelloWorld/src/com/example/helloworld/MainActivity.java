package com.example.helloworld;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("HELLOWORD", "Passo no ONCREATE");
        setContentView(R.layout.outratela);
        
        TimerTask timerTask = new TimerTask() {
			
			@Override
			public void run() {
				Intent i = new Intent(MainActivity.this, 
						outratela.class);
				startActivity(i);
			}
		};
		//classe, objeto e inst�nciqa
		Timer timer = new Timer();
		timer.schedule(timerTask, 2000);
    }
    
    @Override
    protected void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    	Log.i("HELLOWORD", "Passo no ONSTART");
    }
    
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	Log.i("HELLOWORD", "Passo no ONRESUME");
    }
    
    @Override
    protected void onPause() {
    	// TODO Auto-generated method stub
    	super.onPause();
    	Log.i("HELLOWORD", "Passo no ONPAUSE");
    }
    
    @Override
    protected void onStop() {
    	// TODO Auto-generated method stub
    	super.onStop();
    	Log.i("HELLOWORD", "Passo no ONSTOP");
    }
    
    @Override
    protected void onRestart() {
    	// TODO Auto-generated method stub
    	super.onRestart();
    	Log.i("HELLOWORD", "Passo no ONRESTART");
    }
    
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    	Log.i("HELLOWORD", "Passo no ONDESTROY");
    }
    
}
