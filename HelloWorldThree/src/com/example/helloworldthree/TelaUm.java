package com.example.helloworldthree;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

public class TelaUm extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_um);
		
		TimerTask tTask = new TimerTask() {
			
			@Override
			public void run() {
				Intent i = new Intent("cadaumpoeaactivityondebementender");
				startActivity(i);
			}
		};
		
		Timer timer = new Timer();
		timer.schedule(tTask, 6000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_tela_um, menu);
		return true;
	}

}
