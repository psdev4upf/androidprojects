package com.example.helloworldtwo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends Activity {

	private RadioButton rdbMaiordoMundo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		rdbMaiordoMundo = (RadioButton)findViewById(R.id.rdbGremio);
	}
	
	public void proximaTela(View v){
		Intent i = new Intent(this, TelaDois.class);
		i.putExtra("escolha", rdbMaiordoMundo.isChecked());
		startActivity(i);
	}
	
	public void efetuaLigacao(View v){
		String telephone = ((EditText)findViewById(R.id.edtPhone)).getText().toString();
		Intent i = new Intent(Intent.ACTION_CALL);
		i.setData(Uri.parse("tel:"+telephone));
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
