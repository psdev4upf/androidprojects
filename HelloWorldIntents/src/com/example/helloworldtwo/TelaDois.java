package com.example.helloworldtwo;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.LinearLayout;

public class TelaDois extends Activity{
	
	LinearLayout ll;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.teladois);
		
		ll = (LinearLayout)findViewById(R.id.linearRootTela2);
		
		Bundle extras = getIntent().getExtras();
		boolean azul = extras.getBoolean("escolha");
		if (azul){
			ll.setBackgroundColor(Color.BLUE);
		} else {
			ll.setBackgroundColor(Color.RED);
		}
	}

}
