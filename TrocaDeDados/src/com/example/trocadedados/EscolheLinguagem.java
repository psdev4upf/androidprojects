package com.example.trocadedados;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;

public class EscolheLinguagem extends Activity{

	private Linguagem[] linguagens;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.escolhe_linguagem);
		
		linguagens = new Linguagem[5];
		linguagens[0] = new Linguagem("C", "Steve Jobs", 1500, 2, "Todos");
		linguagens[1] = new Linguagem("Java", "James Gosling", 1992, 1, "Todos");
		linguagens[2] = new Linguagem("Pascal", "Zinedine Zidane", 1512, 19, "Idosos");
		linguagens[3] = new Linguagem("Delphi", "Borland", 1980, 10, "Corporativo");
        linguagens[4] = new Linguagem("Assembly", "Ad�o e Eva", 0, 15, "Nerds Xiitas");
	}
	
	public void escolheu(View v){
		Spinner spinner = (Spinner) findViewById(R.id.spLinguagem);
		
		Intent i = new Intent();
		i.putExtra("linguagem_escolhida", linguagens[spinner.getSelectedItemPosition()]);
		setResult(RESULT_OK, i);
		finish();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent i = new Intent();
		setResult(RESULT_CANCELED, i);
		super.onBackPressed();
	}
	
}
