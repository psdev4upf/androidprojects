package com.example.trocadedados;

import java.io.Serializable;

public class Linguagem implements Serializable{

	private static final long serialVersionUID = 2409873727976638708L;
	public String nome;
	public String criador;
	public long anoCriacao;
	public int posicaoRankingTiobe;
	public String alvo;
	
	public Linguagem(String nome, String criador, long anoCriacao,
			int posicaoRankingTiobe, String alvo) {
		super();
		this.nome = nome;
		this.criador = criador;
		this.anoCriacao = anoCriacao;
		this.posicaoRankingTiobe = posicaoRankingTiobe;
		this.alvo = alvo;
	}
	
	
	
}
