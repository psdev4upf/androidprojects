package com.example.trocadedados;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
	public static final int RETORNO_TELA_LINGUAGEM = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void muda(View v){
		Intent i = new Intent(this, EscolheLinguagem.class);
		startActivityForResult(i, RETORNO_TELA_LINGUAGEM);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == RETORNO_TELA_LINGUAGEM && resultCode == RESULT_OK){
			Linguagem txt = (Linguagem)data.getSerializableExtra("linguagem_escolhida");
			((TextView)findViewById(R.id.txt)).setText(txt.nome);
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
