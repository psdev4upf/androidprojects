package com.example.salvaconta;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;

public class MainActivity extends Activity {

	AQuery aq;
	private TextView txtNome;
	private TextView txtSobrenome;
	private ImageView imgThumb;
	
	private SharedPreferences sp;
	private SharedPreferences.Editor spEdit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		aq = new AQuery(this);
		
		txtNome = (TextView) findViewById(R.id.txtNome);
		txtSobrenome = (TextView) findViewById(R.id.txtSobrenome);
		imgThumb = (ImageView) findViewById(R.id.imgThumb);
		
		sp = PreferenceManager.getDefaultSharedPreferences(this);
		spEdit = sp.edit();
		
		if (sp.contains("nome")){
			txtNome.setText(sp.getString("nome", ""));
			txtSobrenome.setText(sp.getString("sobrenome", ""));
			aq.id(imgThumb).image(sp.getString("thumb", ""));
		}
	}

	public void busca(View v){
		EditText edtConta = (EditText) findViewById(R.id.edtContaGPlus);
		aq.ajax("https://www.googleapis.com/plus/v1/people/"+edtConta.getText()
				+"?key=AIzaSyAE21zQiL6sNuhu2IUFCR2plZ-CnZTclps",
				JSONObject.class, 
				new AjaxCallback<JSONObject>(){
					public void callback(String u, JSONObject root, com.androidquery.callback.AjaxStatus status) {
						try {
							JSONObject objName = root.getJSONObject("name");
							String familyName = objName.getString("familyName");
							String givenName = objName.getString("givenName");
							txtNome.setText(givenName);
							txtSobrenome.setText(familyName);
							
							JSONObject objImage = root.getJSONObject("image");
							String url = objImage.getString("url");
							aq.id(imgThumb).image(url);
							
							spEdit.putString("nome", givenName);
							spEdit.putString("sobrenome", familyName);
							spEdit.putString("thumb", url);
							spEdit.commit();
						} catch (JSONException e) {
							// TODO: handle exception
						}
					};
				}
		);
	}

}
