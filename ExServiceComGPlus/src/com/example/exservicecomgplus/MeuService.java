package com.example.exservicecomgplus;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MeuService extends Service{

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		Log.e("SERVICE", "ONCREATE");
		super.onCreate();
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		Log.e("SERVICE", "ONSTART");
		super.onStart(intent, startId);
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//STARTSERVICE
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.e("SERVICE", "ONSTARTCOMMAND");
		// TODO Auto-generated method stub
		TimerTask tTask = new TimerTask() {
			
			@Override
			public void run() {
				int sorteio = (int)(Math.random()*5);
				Log.e("SERVICE", "sorteio: "+sorteio);
				if (sorteio == 3){
					this.cancel();
					stopSelf();
				}
			}
		};
		final Timer t = new Timer();
		t.schedule(tTask, 2000, 2500);
		return 1;
	}
	
	@Override
	public void onDestroy() {
		Log.e("SERVICE", "ONDESTROY");
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
