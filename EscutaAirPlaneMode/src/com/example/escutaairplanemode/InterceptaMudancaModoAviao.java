package com.example.escutaairplanemode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.sax.StartElementListener;
import android.widget.Toast;

public class InterceptaMudancaModoAviao extends BroadcastReceiver{

	@Override
	public void onReceive(Context ctx, Intent arg1) {
		if (isAirPlaneModeOn(ctx)){
			Intent i = new Intent("desligou_modo_aviao");
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			ctx.startActivity(i);
		} else {
			Intent i = new Intent("ligou_modo_aviao");
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			ctx.startActivity(i);
		}
	}
	
	public static boolean isAirPlaneModeOn(Context ctx){
		return Settings.Global.getInt(ctx.getContentResolver(),
				Settings.Global.AIRPLANE_MODE_ON, 0) != 1;
	}
	
}
