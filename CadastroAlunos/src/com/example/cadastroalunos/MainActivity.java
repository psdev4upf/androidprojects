package com.example.cadastroalunos;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

public class MainActivity extends ListActivity {

	private BDManager bdManager;
	private ArrayAdapter<String> adapter;
	private List<String> alunos;
	private int indice;
	
	private ActionMode actionMode;
	private ActionMode.Callback callback = new ActionMode.Callback() {
		
		@Override
		public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
			return false;
		}
		
		@Override
		public void onDestroyActionMode(ActionMode arg0) {
			actionMode = null;
		}
		
		@Override
		public boolean onCreateActionMode(ActionMode arg0, Menu menu) {
			getMenuInflater().inflate(R.menu.menu_contextual, menu);
			return true;
		}
		
		@Override
		public boolean onActionItemClicked(ActionMode arg0, MenuItem mi) {
			if (mi.getItemId() == R.id.mnRemove){
				bdManager.removeAluno(alunos.get(indice));
				alunos.remove(indice);
				refreshList();
			} else {
				Intent i = new Intent(MainActivity.this, AddAluno.class);
				i.putExtra("nome", alunos.get(indice));
				startActivityForResult(i, 11);
			}
			actionMode.finish();
			return true;
		}
	};
	
	protected void onListItemClick(android.widget.ListView l, android.view.View v, 
			int position, long id) {
		if (actionMode == null){
			indice = position;
			actionMode = startActionMode(callback);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		bdManager = new BDManager(this);
		alunos = bdManager.getAlunos();
		
		adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, alunos);
		setListAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.mnAdd){
			Intent i = new Intent(this, AddAluno.class);
			startActivityForResult(i, 10);
		}
		return true;
	}
	
	public void refreshList(){
		adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, alunos);
		adapter.notifyDataSetChanged();
		setListAdapter(adapter);
		getListView().invalidateViews();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 10 && resultCode == RESULT_OK){
			alunos.add(data.getStringExtra("nome"));
			bdManager.insereAluno(data.getStringExtra("nome"));
			refreshList();
		} else if (requestCode == 11 && resultCode == RESULT_OK){
			alunos.set(indice, data.getStringExtra("nome"));
			bdManager.editaAluno(alunos.get(indice), data.getStringExtra("nome"));
			refreshList();
		}
	}

}
