package com.example.cadastroalunos;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDManager extends SQLiteOpenHelper{

	private static final String DB_NAME = "db_alunos";
	private static final int DB_VERSION = 1;
	
	private String TABLE_NAME = "alunos";
	private String FIELD_NAME = "nome";
	
	private String SQL_CREATE_TABLE = "create table " + TABLE_NAME + 
			" (id integer primary key autoincrement, " +
			"nome text not null)";
	
	public BDManager(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_TABLE);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
	
	public void insereAluno(String nome){
		SQLiteDatabase db = getWritableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put(FIELD_NAME, nome);
		
		db.insert(TABLE_NAME, null, cv);
		db.close();
	}
	
	public void editaAluno(String oldName, String newName){
		SQLiteDatabase db = getWritableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put(FIELD_NAME, newName);
		
		db.update(TABLE_NAME, cv, FIELD_NAME + " = ?", new String[]{oldName});
		db.close();
	}
	
	public void removeAluno(String nome){
		SQLiteDatabase db = getWritableDatabase();
		db.delete(TABLE_NAME, FIELD_NAME + " = ?", new String[]{nome});
		db.close();
	}
	
	public List<String> getAlunos(){
		SQLiteDatabase db = getReadableDatabase();
		List<String> alunos = new ArrayList<String>();
		
		Cursor cursor = db.query(TABLE_NAME, new String[]{FIELD_NAME},
				null, null, null, null, FIELD_NAME);
		
		while (cursor.moveToNext()){
			alunos.add(cursor.getString(cursor.getColumnIndex(FIELD_NAME)));
		}
		
		cursor.close();
		db.close();
		return alunos;
	}
	
}
