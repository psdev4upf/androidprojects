package com.example.cadastroalunos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AddAluno extends Activity{

	private EditText edtNome;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_aluno);
		
		edtNome = (EditText) findViewById(R.id.edtNome);
		
		Bundle b = getIntent().getExtras();
		if (b != null){
			edtNome.setText(b.getString("nome"));
		}
	}
	
	public void salvar(View v){
		Intent i = new Intent();
		i.putExtra("nome", edtNome.getText().toString());
		setResult(RESULT_OK, i);
		finish();
	}
	
	public void cancelar(View v){
		setResult(RESULT_CANCELED);
		finish();
	}
	
	@Override
	public void onBackPressed() {
		cancelar(null);
	}
	
}
