package com.example.salvaconta;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;

public class MainActivity extends Activity {

	AQuery aq;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		aq = new AQuery(this);
	}

	public void busca(View v){
		final ImageView img = (ImageView) findViewById(R.id.imgGender);
		EditText edtConta = (EditText) findViewById(R.id.edtContaGPlus);
		aq.ajax("https://www.googleapis.com/plus/v1/people/" +edtConta.getText()+
				"?key=AIzaSyAE21zQiL6sNuhu2IUFCR2plZ-CnZTclps",
				JSONObject.class, 
				new AjaxCallback<JSONObject>(){
					public void callback(String u, JSONObject root, com.androidquery.callback.AjaxStatus status) {
						try {
							String gender = root.getString("gender");
							
							if (gender.equalsIgnoreCase("male")){
								img.setImageResource(R.drawable.menino);
							} else {
								img.setImageResource(R.drawable.menina);
							}
							
							final String url = root.getString("url");
							Button btnVisitar = (Button) findViewById(R.id.btnVisitar);
							btnVisitar.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									Intent i = new Intent(Intent.ACTION_VIEW);
									i.setData(Uri.parse(url));
									startActivity(i);
								}
							});
						} catch (JSONException e) {
							// TODO: handle exception
						}
					};
				}
		);
	}

}
