package com.example.meumapview;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void fazRota(View v){
		EditText edtEndereco = (EditText) findViewById(R.id.edtEndereco);
		
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse("google.navigation:q=" + edtEndereco.getText().toString()));
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
